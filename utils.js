const axios = require('axios');
const fs = require('fs');
const path = require('path');

const getData = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then((res) => {
                resolve(res.data);
            })
            .catch((err) => {
                reject(err);
            })
    })
}

function dumpResults(data, fileName){
    fs.writeFile(path.join(__dirname, `solutions/${fileName}`), JSON.stringify(data,null,"\t"), 'utf-8', 
    (err) => {
        if(err)
            console.log(err);
    });
}

module.exports = {getData, dumpResults};