const fs = require('fs');
const path = require('path');

function readFile(filePath, callback) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err)
                reject('Missing File\n', err);
            else {
                callback(data);
                resolve(data);
            }
        });
    });
}

function dataTransformation(data) {
    const lowerCase = (data) => data.toLowerCase();
    const upperCase = (data) => data.toUpperCase();
    const transform = [lowerCase, upperCase];
    const res = transform.reduce((acc, curr) => {
        acc.push(curr(data));
        return acc;
    }, []);
    console.log(res);
}