const toDosUrl = 'https://jsonplaceholder.typicode.com/todos';
const userTasks = require('./1-C')
const taskStatus = require('./1-D');
const getData = require('./utils').getData;
const dumpResults = require('./utils').dumpResults;
const fetchA = require('./1-A');
const fetchB = require('./1-B');

//Problem 1-A
fetchA(toDosUrl, dumpResults);

//Problem 1-B
fetchB(toDosUrl, dumpResults);

//Problem 1-C
getData(toDosUrl).then((data) => {
    dumpResults(userTasks(data), 'solutionC.json');
});

//Problem 1-D
getData(toDosUrl).then((data) => {
    dumpResults(taskStatus(data), 'solutionD.json');
});