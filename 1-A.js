const getData = require('./utils.js').getData;

// A. using promises
function fetchA(url, callback) {
    getData(url)
        .then((data) => {
            callback(data, 'solutionA.json');  
        })
        .catch((err) => {
            console.log(err);
        })
}
module.exports = fetchA;



