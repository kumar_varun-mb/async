
function userTasks(data) {
    let res = data.reduce((acc, curr) => {
        let userId = curr.userId;
        let taskId = curr.id;
        let title = curr.title;
        let completed = curr.completed;
        let task = {
            'Task ID': taskId,
            'Title': title,
            'Completed': completed
        }
        if (!(Array.isArray(acc[userId])))
            acc[userId] = [];
        if (completed)
            acc[userId].push(task);
        else
            acc[userId].unshift(task);
        return acc;
    }, {});
    return res;
}

module.exports = userTasks;