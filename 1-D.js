
function taskStatus(data) {
    let res = data.reduce((acc, curr) => {
        let userId = curr.userId;
        let taskId = curr.id;
        let status = 'Not Completed' ;
        let title = curr.title;
        if (curr.completed == true)
            status = 'Completed';
        let task = {
            'Task ID': taskId,
            'Title': title
        }
        if(!acc[status])
            acc[status] = {};
        if(!acc[status][userId])
            acc[status][userId] = [];
        acc[status][userId].push(task);
        return acc;
    }, {});
    return (res);
}

module.exports = taskStatus;

