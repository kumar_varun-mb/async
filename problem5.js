const sayHelloWorld = () => {
    return new Promise((resolve, reject) => {
        window.setTimeout(() => {
            resolve('Hello World');
        }, 1000)
    })
}
(function executeSayHelloWorld() {
    sayHelloWorld().then((message) => {
        console.log(message);
        console.log('Hey');
    });
})()