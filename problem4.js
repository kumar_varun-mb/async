const axios = require("axios");
const userIds = [1, 2, 3, 4, 5, 6];

function fetchUserData(url) {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then((result) => {
                resolve(result.data);
            })
            .catch((err) => {
                reject(err);
            })
    })
}
const urlAppend = (url, userIds) => {
    return new Promise((resolve, reject) => {
        let resUrl = userIds.reduce((acc, curr) => {
            acc += '&id=' + curr;
            return acc;
        }, url + '?');
        resolve(resUrl);
    })
}

urlAppend('https://jsonplaceholder.typicode.com/users', userIds)
    .then((url) => {
        return fetchUserData(url);
    })
    .then((data) => {
        console.log(data);
    })