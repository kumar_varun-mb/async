const getData = require('./utils.js').getData;

// B. using await keyword
async function fetchB(url, callback) {
    try {
        const data = await getData(url);
        callback(data, 'solutionB.json');
    }
    catch (err) {
        console.log(err);
    }
}
module.exports = fetchB;