// Q3. Write a function to get all the users information from the users API url. Find all the users with name "Nicholas". 
// Get all the to-dos for those user ids.
const usersUrl = 'https://jsonplaceholder.typicode.com/users';
const toDosUrl = 'https://jsonplaceholder.typicode.com/todos';
const getData = require('./utils').getData;

function users(data, name) {
    return new Promise((resolve, reject) => {
        let userIds = data.reduce((acc, curr) => {
            if (curr.name.includes(name))
                acc.push(curr.id);
            return acc;
        }, []);
        resolve(userIds);
    })
}
function todos(data, userIds) {
    return new Promise((resolve, reject) => {
        let res = data.reduce((acc, curr) => {
            if (userIds.includes(curr.userId)) {
                if (!acc[curr.userId])
                    acc[curr.userId] = [];
                let task = {
                    'task id': curr.id,
                    'title': curr.title
                }
                acc[curr.userId].push(task);
            }
            return acc;
        }, {})
        resolve(res);
    })
}

getData(usersUrl)
    .then((usersData) => {
        return users(usersData, 'Nicholas');
    })
    .then((userIds) => {
        getData(toDosUrl)
            .then((todosData) => {
                return todos(todosData, userIds)
            })
            .then((res) => {
                console.log(res);
            })
    })